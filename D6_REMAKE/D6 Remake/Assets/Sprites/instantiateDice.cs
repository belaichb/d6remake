﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class instantiateDice : MonoBehaviour
{
    int i;

    public GameObject dice;
    public GameObject[] topRow;
    public GameObject[] bottomRow;
    public GameObject selectedDie;
    Vector2[] topRowPositions;
    Vector2[] bottomRowPositions;

    Vector2 diceHolderLocation = new Vector2(0, -3);

    public float diceManagementNum;

    public float selectedNumber;
    public float selectedColor;
    public float comparisonColor;
    public float comparisonNumber;

    //scoreing
    int maxHealth = 6;
    public int currentHealth;
    public int streak;
    public int bestStreak;
    public int score;

    public Text scoreTxt;
    public Text lifeTxt;
    public Text gameOverText;

    bool gameOver = false;

    void Awake()
    {
        gameOverText.text = "";

        topRow = new GameObject[5];
        bottomRow = new GameObject[5];
        i = 0;

        topRowPositions = new Vector2[5];
        bottomRowPositions = new Vector2[5];

        while (i < 5)
        {
            topRowPositions[i] = new Vector2(i * 2.3f -4.6f, 1);
            bottomRowPositions[i] = new Vector2(i * 2.3f -4.6f, -1);
            i++;
        }
        i = 0;

        while (i < 5)
        {
            GameObject newDiceTop = Instantiate(dice, topRowPositions [i], transform.rotation, transform);
            newDiceTop.SendMessage("SetRow", i + 1);
            topRow[i] = newDiceTop;

            GameObject newDiceBottom = Instantiate(dice, bottomRowPositions [i] , transform.rotation, transform);
            newDiceBottom.SendMessage("SetRow", i + 1);
            newDiceBottom.SendMessage("SetActive");
            bottomRow[i] = newDiceBottom;
            i++;
        }

    }
    // Start is called before the first frame update
    void Start()
    {

        currentHealth = maxHealth;
}

  
    // Update is called once per frame
    void Update()
    {
        if (gameOver == false)
        {
            scoreTxt.text = "Score: " + score + " Streak:" + streak + " Best streak:" + bestStreak;
            lifeTxt.text = "Remaining Health:" + currentHealth;

            if (streak > bestStreak)
            {
                bestStreak = streak;
            }

            if (selectedDie == null)
            {

            }
            if (currentHealth == 0)
            {
                BroadcastMessage("KillObject");
                gameOver = true;
            }
        }
        else
        {
            gameOverText.text = "Game Over";

        }
    }

    void diceManagement(int givenRowNumber)
    {
        BroadcastMessage("GoDown", givenRowNumber);
        if (selectedDie != null)
        {
            selectedDie.SendMessage("KillObject");
        }
        selectedDie = bottomRow[givenRowNumber - 1];
        bottomRow[givenRowNumber - 1] = topRow[givenRowNumber - 1];
        GameObject newDice = Instantiate(dice, new Vector2(topRowPositions[givenRowNumber-1].x, topRowPositions[givenRowNumber - 1].y + 7), transform.rotation, transform);
        topRow[givenRowNumber-1] = newDice;

        newDice.SendMessage("SetRow", givenRowNumber);

    }
    void SetNewSelected(Vector2 colorAndNumber)
    {
        selectedColor = colorAndNumber.x;
        selectedNumber= colorAndNumber.y;
    }

    void SetNewComparison(Vector2 colorAndNumber)
    {
        comparisonColor = colorAndNumber.x;
        comparisonNumber = colorAndNumber.y;
    }
    void Test()
    {
        if (Mathf.RoundToInt(selectedColor) == Mathf.RoundToInt(comparisonColor))
        {
            BroadcastMessage("TestResults", true);
        } else if (Mathf.RoundToInt(selectedNumber) == Mathf.RoundToInt(comparisonNumber - 1)){
            BroadcastMessage("TestResults", true);
        }
        else if (Mathf.RoundToInt(selectedNumber) == 6)
        {
            if (Mathf.RoundToInt(comparisonNumber) == 1)
            {
                BroadcastMessage("TestResults", true);
            }
        }
         if(selectedDie == null)
        {
            BroadcastMessage("TestResults", true);
        }
        BroadcastMessage("TestResults", false);
    }

    //Scoring
    void LoseLife()
    {
        currentHealth--;
        streak = 0;
    }

    void IncreaseScore()
    {
        score++;
        streak++;
    }
    
}