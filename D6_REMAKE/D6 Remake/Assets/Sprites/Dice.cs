﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    public bool isActive = false;
   public bool isSelected = false;
    public int rowNumber;
    bool getDestroyed = false;
    bool checkForTest = false;

    //Define color and number ranges
    int color;
    int number;
    int i;

    //Define colors 
   public Material purple;
    public Material cyan;
    public Material red;
    public Material pink;
    public Material orange;
    public Material green; 

    //Define numbers
        public Sprite one;
        public Sprite two;
        public Sprite three; 
        public Sprite four; 
        public Sprite five;
        public Sprite six;

    public GameObject background;
    public GameObject numberChild;

    SpriteRenderer spriteRendererComponentBackground;
    SpriteRenderer spriteRendererComponentNumber;

    public float percent;

    public Vector2 startPosition;
    public Vector2 endPosition;
    Vector2[] topRowPositions;
    Vector2[] bottomRowPositions;
    Vector2 selectedDicePosition;





    // Start is called before the first frame update
    void Awake()
    {
        selectedDicePosition =  new Vector2(0.2f, -3);
        //Get sprites for numbers and colors
        spriteRendererComponentNumber = numberChild.GetComponent<SpriteRenderer>();
        spriteRendererComponentBackground = background.GetComponent<SpriteRenderer>();

        //Define the random settings for number and color
        color = Random.Range(1, 7);
        number = Random.Range(1, 7);

        // setting up position arrays
        topRowPositions = new Vector2[5];
        bottomRowPositions = new Vector2[5];
        while (i < 5)
        {
            topRowPositions[i] = new Vector2(i * 2.3f - 4.6f, 1);
            bottomRowPositions[i] = new Vector2(i * 2.3f - 4.6f, -1);
            i++;
        }
        i = 0;

        //Define the colors for the background
        if (color == 1)
        {

          spriteRendererComponentBackground.material = purple; 
            
        } else if (color == 2)
        {

            spriteRendererComponentBackground.material = cyan;

        } else if (color == 3)
        {

            spriteRendererComponentBackground.material = red;

        } else if (color == 4)
        {
            spriteRendererComponentBackground.material = pink;

        } else if (color == 5)
        {

            spriteRendererComponentBackground.material = orange;  

        } else if (color == 6)
        {

            spriteRendererComponentBackground.material = green;
        }

        //Define the numbers of dice 
        if (number == 1)
        {
            spriteRendererComponentNumber.sprite = one;

        } else if (number == 2)
        {
            spriteRendererComponentNumber.sprite = two;

        } else if (number == 3)
        {

            spriteRendererComponentNumber.sprite = three;

        } else if (number == 4)
        {

            spriteRendererComponentNumber.sprite = four;

        } else if (number == 5)
        {

            spriteRendererComponentNumber.sprite = five;

        } else if (number == 6)
        {

            spriteRendererComponentNumber.sprite = six;
        }

    }
     void Start()
    {

        startPosition = transform.position;
        

    }

    // Update is called once per frame
    void Update()

    {


        if (rowNumber != 0)
        {
            if (isActive == false && isSelected == false && getDestroyed == false)
            {
                endPosition = topRowPositions[rowNumber - 1];
            }
            if (isActive && getDestroyed == false)
            {
                endPosition = bottomRowPositions[rowNumber - 1];
            }
            if (isSelected && getDestroyed == false)
            {
                endPosition = selectedDicePosition;
            }
        }
        transform.position = Vector2.Lerp(startPosition, endPosition, percent);
        percent += 0.05f; 

        if (isSelected)
        {
            isActive = false;
        }
        //get rid of object
        if (getDestroyed && percent > 1)
        {
            Destroy(gameObject);
        }
    }

    void SetActive()
    {
        isActive = true;
    }
    void SetRow(int number)
    {
        rowNumber = number;
    }

    private void OnMouseDown()
    {
        if (isActive && isSelected == false)
        {
            SendMessageUpwards("SetNewComparison", new Vector2(color, number));
            checkForTest = true;
            SendMessageUpwards("Test");
        }

        if (isSelected == true && percent > 1)
        {
            percent = 0;
            startPosition = transform.position;
            SendMessageUpwards("LoseLife");
            KillObject();
            
        }
    }

    Vector2 FindNextPosition()
    {
        startPosition = transform.position;
        percent = 0;
        Vector2 newDestination = new Vector2(0, 0);
        if (isActive == false && transform.position.y > 2) //is above top tow
        {
            newDestination = topRowPositions[rowNumber-1];
        }else if (isActive == false) //is top row
        {
            newDestination = bottomRowPositions[rowNumber-1];
            isActive = true;
        } else if (isActive == true && isSelected == false)
        {
            newDestination = selectedDicePosition;
        }
        if (isSelected)
        {
            newDestination = new Vector2(4.6f, -10);
        }
        return newDestination;
    }

    void GoDown(int calledRow)
    {
        if(rowNumber == calledRow)
        {
            endPosition = FindNextPosition();
            
        }
        if(isSelected)
        {
            endPosition = FindNextPosition();
        }
    }

    void KillObject()
    {
        percent = 0;
        endPosition = new Vector2(selectedDicePosition.x, -10);
        getDestroyed = true;
    }
    void SetAsSelected()
    {
        percent = 0;
        endPosition = FindNextPosition();
        SendMessageUpwards("diceManagement", rowNumber);
        isSelected = true;
        SendMessageUpwards("SetNewSelected", new Vector2(color, number));
        SendMessageUpwards("IncreaseScore");
    }

    void TestResults(bool result)
    {
        if (checkForTest)
        {
            if (result)
            {
                SetAsSelected();
          

            }
        }
        checkForTest = false;
    }

}
